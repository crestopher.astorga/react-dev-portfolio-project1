import React from 'react';
import NavBar from '../components/Nav/NavBar';
import Footer from '../components/Footer';

function Contact() {
    return (
        <section class="py_80 bg_black full_row contact">
        <NavBar />
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<div class="section_title_1 text-center mx-auto pb_60 wow animated slideInUp">
	                    <h2 class="title text-uppercase color_white">Get In Touch</h2>
	                    <span class="sub_title">You can contact me using the following details on the side section of this area, <br /> Or you can send me a message by filling out the form below.</span>
	                </div>
				</div>
				<div class="col-md-12 col-lg-12 ">
					<div class="row">
						<div class="col-md-4 col-lg-4">
							<div class="contact_info wow animated fadeInLeft">
		    					<ul>
		    						<li>
		    							<div class="contact_text">
		    								<h6 class="font-weight-bold color_white">Email</h6>
		    								<span class="color_lightgray">crestopher.astorga@outlook.com</span>
		    							</div>
		    						</li>
		    						<li>
		    							<div class="contact_text">
		    								<h6 class="font-weight-bold color_white">Phone</h6>
		    								<span class="color_lightgray">+639109957119</span>
		    							</div>
		    						</li>
		    						<li>
		    							<div class="contact_text">
		    								<h6 class="font-weight-bold color_white">Address</h6>
		    								<span class="color_lightgray">Caloocan City, Metro Manila</span>
		    							</div>
		    						</li>
		    					</ul>
		    				</div>
						</div>
						<div class="col-md-8 col-lg-8">
							<form class="form contact_message wow animated fadeInRight" id="contact-form" action="contactform.php" method="post">
								<div class="row">
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<input class="form-control bg_black" type="text" name="name" placeholder="Your Name" />
										</div>
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
											<input class="form-control bg_black" type="email" name="email" placeholder="Email Address" />
										</div>
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
										  <input class="form-control bg_black" type="text" name="subject" placeholder="Subject"/>
										</div>
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<textarea class="form-control bg_black" name="message" rows="7" placeholder="Message"></textarea>
										</div>							
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<input className="btn btn-default" id="send" value="Send Massage" type="submit"/>
										</div>
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="error-handel">
											<div id="success">Your email sent Successfully, Thank you.</div>
											<div id="error"> Error occurred while sending email. Please try again later.</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    <Footer />
	</section>  
    )
}

export default Contact;
