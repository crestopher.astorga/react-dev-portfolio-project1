import React from 'react';
import NavBar from '../components/Nav/NavBar';
import Footer from '../components/Footer';

function About() {
    return (
        <section className="py_80 bg_deepblack full_row skills">
        <NavBar />
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<div class="section_title_1 text-center mx-auto pb_60 wow animated slideInUp">
	                    <h2 class="title text-uppercase color_white">My Skills</h2>
	                    <span class="sub_title color_lightgray">Below are my skills and the rate of my expertise.</span>
	                </div>
				</div>
			</div>
			<div class="my_skill">
				<div class="row">
					<div class="col-md-12 col-lg-6">
						<div class="about_myskill color_secondery wow animated slideInLeft">
							<h2 class="color_white">Here are the list of my Web Design <br />& Developer Skills</h2>
							<p class="pt_15">I was amazed how the web pages works when I first encountered it, and got curious on how it is being built or developed. I did a self study since high school and built static web pages, and played around with CSS on making the pages more attractive or presentable. I did side jobs coding for the companies that I have worked for to help enhance the looks of thier existing internal tools, and also with the help of my co-workers who is also familiar with coding.</p>
							<p class="pt_15">Then I decided to have it my fulltime job so I enrolled to a Bootcamp program. I have completed courses on Udemy and got certificates but for me, there is still a big difference learning with live instructors and with classmates to collaborate with. I was right because I have learned the full picture of the web development world. It's a fast paced learning style but structured. I have learned the Front End and Back End technologies by joining the Bootcamp.</p>
						</div>
					</div>
					<div class="col-md-12 col-lg-6">
						<div class="skill-progress wow animated slideInRight color_lightgray">
		                    <div class="prgs-bar fact-counter"> <span>HTML</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="95">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemax="100"> </div>
								</div>
							</div>
		                    <div class="prgs-bar fact-counter"> <span>CSS</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="95">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemax="100"> </div>
								</div>
							</div>
							<div class="prgs-bar fact-counter"> <span>Bootstrap</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="85">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemax="100"> </div>
								</div>
							</div>							
		                    <div class="prgs-bar fact-counter"> <span>JQuery</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="85">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemax="100"> </div>
								</div>
							</div>
		                    <div class="prgs-bar fact-counter"> <span>JavaScript</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="80">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemax="100"> </div>
								</div>
							</div>
							<div class="prgs-bar fact-counter"> <span>ReactJs</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="85">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemax="100"> </div>
								</div>
							</div>
							<div class="prgs-bar fact-counter"> <span>NodeJs</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="80">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemax="100"> </div>
								</div>
							</div>
							<div class="prgs-bar fact-counter"> <span>MongoDB</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="85">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemax="100"> </div>
								</div>
							</div>
		                    <div class="prgs-bar fact-counter"> <span>Wordpress</span>
		                        <div class="progress count wow" data-wow-duration="0ms">
		                            <div class="skill-percent"><span class="count-num" data-speed="3000" data-stop="90">0</span>%</div>
		                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemax="100"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <Footer />
	</section>
    )
}

export default About;
