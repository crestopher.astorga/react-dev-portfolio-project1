import React from 'react';
import NavBar from '../components/Nav/NavBar';
import Footer from '../components/Footer';
import AboutPic from '../img/about1.png';

function About() {
    return (
        <section class="py_80 bg_black full_row aboutpage">
        <NavBar />
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<div class="section_title_1 text-center mx-auto pb_60 wow animated slideInUp">
	                    <h2 class="title text-uppercase color_white">ABOUT MYSELF</h2>
	                </div>
				</div>
			</div>
			<div class="about_one">
				<div class="row">
					<div class="col-md-7 col-lg-7">
						<div class="myself color_lightgray wow animated fadeInLeft">
							<p class="pt_15">I have been in the Technical Industry for almost 9 years now. I started my career as a Software support for Norton Company, then transitioned to the BPO industry as an offshore Technical Support. I had supported international companies remotely from small businesses to large enterprises as a Technical Support.</p>
							
							<p class="pt_15">Then I pursued my career in the IT industry as it is related to my academic degree, then eventually merged my Technical and Coding Skills. I used to develop software to merge internal tools for easy access like one stop shop, to make everyone's task a lot faster and for easy navigation. I kept on offering my coding skills for almost all of the IT companies that I have worked for.</p>

							<p class="pt_15">Then I decided to be a fulltime Sotware Developer so I joined a Bootcamp program to learn the entire picture of the the Web Development world. It is a fast paced way of learning, but the good thing about Bootcamp, is you will be able to learn the structure of the Web Development. I have learned the different technologies that are being used for the Front End and the Back End, which is a big factor for me to have for being a Software Developer.</p>
						</div>
						<div class="personal_info color_lightgray">
							<div class="row">
								<div class="col-md-12 col-lg-6">
									<ul>
										<li><span class="color_secondery">Name :</span> Chris Astorga</li>
										<li><span class="color_secondery">Email :</span>  crestopher.astorga@outlook.com</li>
										<li><span class="color_secondery">Phone :</span> +639109957119</li>	
									</ul>
								</div>
								<div class="col-md-12 col-lg-6">
									<ul>
										<li><span class="color_secondery">Job Type :</span> Freelancer</li>
										<li><span class="color_secondery">What I Do :</span> Web Designer & Developer</li>
										<li><span class="color_secondery">Address :</span> Caloocan City, Metro Manila</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5 col-lg-5">
						<div class="profile_img  wow animated fadeInRight">
							<img src={AboutPic} alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
    <Footer />
	</section>
    )
}

export default About;
