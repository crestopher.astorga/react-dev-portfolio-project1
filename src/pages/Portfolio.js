import React from 'react';
import NavBar from '../components/Nav/NavBar';
import Footer from '../components/Footer';

function Portfolio() {
    return (
        <section className="py_80 bg_deepblack full_row portfolio">
        <NavBar />
		<div className="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<div class="section_title_1 text-center mx-auto pb_60 wow animated slideInUp">
	                    <h2 class="title text-uppercase color_white">Recent Projects</h2>
	                    <span class="sub_title color_lightgray">Below are the recent projects that I developed for my clients and the other is a demo Enrollment System that I am currently building.</span>
	                </div>
				</div>
				<div class="col-md-12 col-lg-12">
					<div class="my_portfolio" id="tab-panel"> 
              			
						<div class="row">
							<div class="col-md-12">
								<div class="filters mb_30 w-100 text-center color_lightgray">
									<ul class="filter-tabs mx-auto d-inline-block">		
									</ul>
								</div>
							</div>
						</div>	
			              
						<div class="filter-list">
							<div className="portfolio-items">
								<div class="row">
								  	<figure class="portfolio-items bh column mix mix_all graphic development wordpress mb_30 col-md-5 col-lg-3">
										<div class="default-portfolio-item">
											<a href="http://buffalohypnosis.com/" rel="noreferrer" target="_blank">
												<div class="overlay-box">
												<figcaption className="caption">
													<h2>Buffalo Hypnosis</h2>
												</figcaption>
												</div>
											</a>
										</div>
									</figure>
									<figure class="portfolio-items hfm column mix mix_all graphic development wordpress mb_30 col-md-5 col-lg-3">
										<div class="default-portfolio-item">
											<a href="https://hybridfacemaskcompany.com/" rel="noreferrer" target="_blank">
												<div class="overlay-box">
												<figcaption className="caption">
													<h2>Hybrid FaceMasks</h2>	
												</figcaption>
												</div>
											</a>
										</div>
									</figure>
									<figure class="portfolio-items es column mix mix_all graphic development wordpress mb_30 col-md-5 col-lg-3">
										<div class="default-portfolio-item">
											<a href="https://crestopher.astorga.gitlab.io/front-end-project/" rel="noreferrer" target="_blank">
												<div class="overlay-box">
												<figcaption className="caption">
													<h2>A <br />Academy</h2>
												</figcaption>
												</div>
											</a>
										</div>
									</figure>
								</div>
							</div>
						</div>		
			        </div>
				</div>
			</div>
		</div>
    <Footer />
	</section>
    )
}

export default Portfolio;
