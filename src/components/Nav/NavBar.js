import React from 'react'
//we need to import the necessary bootstrap components in order for us to utilize the pre-settled bootstrap
import styled from 'styled-components'
import Burger from './Burger'
import Logo from '../../img/logo.png'
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';

//for us to be able to do the task
//step1. We need to acquire the userContext module for us to be able to consume the data and destructure the information it holds

const Nav = styled.nav`
  color: crimson;
  width: 100%;
  height: 47px;
  padding: 0 3px;
  padding-top: 5px;
  display: flex;
  justify-content: space-around;

  .logo {
    padding: 15px 0;
    color: whitesmoke;
    text-decoration: none;
    font-size: 1.3rem;
  }
`


export default function NavBar(){
    return(
      <Nav>
        <img src={Logo} clasName="logo" alt=""/>
          {/* <a href="/" className="logo">CHRIS<span className="lastName">ASTORGA</span></a>        */}
        <Burger />
      </Nav>
    )
}
