import React, { useState } from 'react'
//we need to import the necessary bootstrap components in order for us to utilize the pre-settled bootstrap
import styled from 'styled-components'
import RightNav from './RightNav'


//for us to be able to do the task
//step1. We need to acquire the userContext module for us to be able to consume the data and destructure the information it holds
const StyledBurger = styled.div`
    width: 2rem;
    height: 2rem;
    position: sticky;
    top: 15px;
    right: 20px;
    z-index: 20;
    display: none;

    @media (max-width: 768px) {
        display: flex;
        justify-content: space-around;
        flex-flow: column nowrap;
    }

    div {
        width: 2rem;
        height: 0.25rem;
        background-color: ${({open}) => open ? 'crimson' : 'whitesmoke'};
        border-radius: 10px;
        transform-origin: 1px;
        transition: all 0.3s linear;

        &:nth-child(1) {
            transform: ${({open}) => open ? 'rotate(45deg)' : 'rotate(0)'};
            
        }

        &:nth-child(2) {
            transform: ${({open}) => open ? 'translateX(100%)' : 'translateX(0)'};
            opacity: ${({open}) => open ? '0' : '1'};
        }

        &:nth-child(3) {
            transform: ${({open}) => open ? 'rotate(-45deg)' : 'rotate(0)'};
        }
    }
`

export default function Burger(){
const [open, setOpen] = useState(false)

    return(
        <>
            <StyledBurger open={open} onClick={() => setOpen(!open)}>
                <div />
                <div />
                <div />
            </StyledBurger>
            <RightNav open={open}/>
        </>
    )
}
