import React from 'react'
//we need to import the necessary bootstrap components in order for us to utilize the pre-settled bootstrap
import styled from 'styled-components'
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';

//for us to be able to do the task
//step1. We need to acquire the userContext module for us to be able to consume the data and destructure the information it holds
const UL = styled.ul`
    list-style: none;
    display: flex;
    flex-flow: row nowrap;
    z-index: 10;

 li {
    padding: 18px 10px;
  }

  li a {
    font-weight: 500;
    font-size: .8rem;
    color: #A9A9A9;
    text-decoration: none;
    letter-spacing: .1rem;
  }

  li a:hover {
    color: whitesmoke;
    cursor: pointer;
  }

  @media (max-width: 768px) {
      flex-flow: column nowrap;
      background-color: black;
      position: fixed;
      transform: ${({open}) => open ? 'translateX(0)' : 'translateX(100%)'};
      top: 0;
      width: 100%;
      height: 100%;
      text-align: center;
      line-height: 5rem;
      padding-top: 3.5rem;
      transition: transform 0.3s ease-in-out;

      li {
          color: whitesmoke;
      }
  }
`

export default function RightNav({open}){
    return(
        <UL open={open}>
          <li><a href="/">HOME</a></li>
          <li><a href="/about">ABOUT</a></li>
          <li><a href="/skills">SKILLS</a></li>
          <li><a href="/portfolio">PORTFOLIO</a></li>
          <li><a href="/contact">CONTACT</a></li>
        </UL>
    )
}
