import React from 'react';
import Container from 'react-bootstrap/Container';
//now we are going to acquire a Jumbotron component frm our react-bootstrap package
import Button from 'react-bootstrap/Button';
import Footer from '../components/Footer';
import NavBar from '../components/Nav/NavBar';

// import { Link } from 'react-router-dom';

//next thing that we are going to do is create a function to return our Banner component


export default function Landing(){
    //lets destructure the props of the data error into it's designated properties
    return(
        <div className="hero">
        <NavBar />
            <Container className="hero-text">
                <h1>I'm <span className="name">Chris Astorga</span></h1>
                <p>Developing and designing web pages is my passion. <br />It makes me feel great  and fullfilled when building something<br /> that makes people lives easier.</p>

                <Button variant="danger" type="submit"><a href="/resume.pdf" download>Download CV</a></Button>               
            </Container>
            <Footer />
        </div>
    )
}