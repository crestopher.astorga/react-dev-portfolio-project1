import React from "react";

export default function Footer() {
    return(
  <div className="footer">
    <p>&copy; 2020. All Rights Reserved. Developed by Chris Astorga</p>
  </div>
    );
};