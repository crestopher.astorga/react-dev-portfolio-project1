import React from 'react';
import Home from './pages/Home';
import About from './pages/About'; //it can be repackaged
import Skills from './pages/Skills';
import Portfolio from './pages/Portfolio';
import Contact from './pages/Contact';
import './App.css';
//routing components
import { BrowserRouter as Router } from 'react-router-dom';//we are going to acquire the react-router-dom package. 
//is it necessary or required to rename it as "Router", this just an extra step (OPTIONAL)
import { Route, Switch } from 'react-router-dom'; //these components combined ared going to tell react what components/pages should be rendered on a specific URL. 



function App() { 
  return (
  	/*
		this is all the components inside the entry point.
  	*/
    <div> 
  {/*this STEP 8: wrap the component tree within the userProvider context provider so that the components will have access to the passed values in here */} 
      <Router>  
        <Switch> 
      {/*When we try to load a page with a URL similar to another component, you are taking the risk of rendering the wrong page... so the switch component is looking for the first match, but as soon as react finds "/", it automatically renders the Home component*/}
          {/*let's use the Route Component to render components within this container based on the defined routes that we will assign to them.*/}
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/skills" component={Skills} />
          <Route path="/portfolio" component={Portfolio}/>
          <Route path="/contact" component={Contact}/>
        </Switch>
      </Router> 
    </div>
  );
}

export default App;
